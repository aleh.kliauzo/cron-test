# cron-test

## Build
Navigate to project root dir (directory containing cron-test.sln)\
Run `dotnet build`

## Run
Navigate to project root dir (directory containing cron-test.sln)\
Run `dotnet run --project ./cron-test/cron-test.csproj "*/15 0 1,15 * 1-5 /usr/bin/find"`
