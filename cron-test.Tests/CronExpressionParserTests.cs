﻿using FluentAssertions;
using System;
using Xunit;

namespace cron_test.Tests
{
    public class CronExpressionParserTests
    {
        [Fact]
        public void ShouldParseTestExpression()
        {
            // Arrange
            var expression = $"*/15 0 1,15 * 1-5 /usr/bin/find";

            // Act
            var res = CronExpressionParser.Parse(expression);

            // Assert
            res.Should().NotBeNull();
            res.Minutes.Should().BeEquivalentTo(new int[] { 0, 15, 30, 45 });
            res.Hours.Should().BeEquivalentTo(new int[] { 0 });
            res.Days.Should().BeEquivalentTo(new int[] { 1, 15 });
            res.Months.Should().BeEquivalentTo(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            res.DaysOfTheWeek.Should().BeEquivalentTo(new int[] { 1, 2, 3, 4, 5 });
            res.Command.Should().BeEquivalentTo("/usr/bin/find");
        }

        [Fact]
        public void ShouldParseTokensWithMultipleCommasAndDashes()
        {
            // Arrange
            var expression = $"1,5,10 2,4-6 1-3,12 1,2-5,8,9-11 1-3 /usr/bin/find";

            // Act
            var res = CronExpressionParser.Parse(expression);

            // Assert
            res.Should().NotBeNull();
            res.Minutes.Should().BeEquivalentTo(new int[] { 1, 5, 10 });
            res.Hours.Should().BeEquivalentTo(new int[] { 2,4, 5, 6 });
            res.Days.Should().BeEquivalentTo(new int[] { 1, 2, 3, 12 });
            res.Months.Should().BeEquivalentTo(new int[] { 1, 2, 3, 4, 5, 8, 9, 10, 11 });
            res.DaysOfTheWeek.Should().BeEquivalentTo(new int[] { 1, 2, 3 });
        }

        [Fact]
        public void ShouldParseCommandsWithWhitespaces()
        {
            // Arrange
            var expression = $"*/15 0 1,15 * 1-5 '/usr/bin/find -f \"text\"'";

            // Act
            var res = CronExpressionParser.Parse(expression);

            // Assert
            res.Should().NotBeNull();
            res.Command.Should().BeEquivalentTo("'/usr/bin/find -f \"text\"'");
        }

        [Fact]
        public void ToStringShouldPrintOnSeparateLines()
        {
            // Arrange
            var expression = $"*/15 0 1,15 * 1-5 '/usr/bin/find -f \"text\"'";

            // Act
            var expr = CronExpressionParser.Parse(expression);
            var res = expr.ToString();

            // Assert
            var newLine = Environment.NewLine;
            res.Split(newLine).Length.Should().Be(7);
        }
    }
}
