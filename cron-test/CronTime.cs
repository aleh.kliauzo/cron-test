﻿using System;

namespace cron_test
{
    public class CronTime
    {
        public int[] Minutes { get; set; }
        public int[] Hours { get; set; }
        public int[] Days { get; set; }
        public int[] Months { get; set; }
        public int[] DaysOfTheWeek { get; set; }
        public string Command { get; set; }

        public override string ToString()
        {
            const int nameChars = -14;
            var minutes = $"{"minute",nameChars}{string.Join(' ', Minutes)}";
            var hours = $"{"hour",nameChars}{string.Join(' ', Hours)}";
            var days = $"{"day of month",nameChars}{string.Join(' ', Days)}";
            var months = $"{"month",nameChars}{string.Join(' ', Months)}";
            var daysOfWeek = $"{"day of week",nameChars}{string.Join(' ', DaysOfTheWeek)}";
            var command = $"{"command",nameChars}{Command}";
            var nl = Environment.NewLine;
            return $"{minutes}{nl}{hours}{nl}{days}{nl}{months}{nl}{daysOfWeek}{nl}{command}{nl}";
        }
    }
}
