﻿using System;

namespace cron_test
{
    class Program
    {
        static void Main(string[] args)
        {
            var expression = CronExpressionParser.Parse(args[0]);
            Console.WriteLine(expression.ToString());
        }
    }
}
