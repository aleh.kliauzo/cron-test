﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace cron_test
{
    public static class CronExpressionParser
    {
        public static CronTime Parse(string cronExpression)
        {
            var regex = new Regex(@"(\S*)\s(\S*)\s(\S*)\s(\S*)\s(\S*)\s(.*)");
            var match = regex.Match(cronExpression);
            var parsedGroups = new List<int[]>();

            for (var i = 1; i < match.Groups.Count - 1; i++)
            {
                var timeGroup = match.Groups[i];
                var parsedGroup = ParseTimeGroup(timeGroup.Value, i).Distinct().OrderBy(x => x).ToArray();
                parsedGroups.Add(parsedGroup);
            }

            var cronTime = new CronTime
            {
                Minutes = parsedGroups[0],
                Hours = parsedGroups[1],
                Days = parsedGroups[2],
                Months = parsedGroups[3],
                DaysOfTheWeek = parsedGroups[4],
                Command = match.Groups[match.Groups.Count - 1].Value
            };

            return cronTime;
        }

        private static int[] ParseTimeGroup(string capture, int index)
        {
            const string everyXExpession = "*/";

            if (capture.Contains(","))
            {
                return capture.Split(",").Select(x => ParseTimeGroup(x, index)).SelectMany(x => x).ToArray();
            }
            else if (capture.Contains("-"))
            {
                var dashIndex = capture.IndexOf("-");
                var rangeStart = ParseSingleValue(capture.Substring(0, dashIndex), index);
                var rangeEnd = ParseSingleValue(capture.Substring(dashIndex + 1), index);

                if (rangeEnd > rangeStart)
                {
                    return Enumerable.Range(rangeStart, rangeEnd - rangeStart + 1).ToArray();
                }
                else
                {
                    return GetRangeValues(index).Where(x => x >= rangeStart || x <= rangeEnd).ToArray();
                }
            }
            else if (capture.Contains(everyXExpession))
            {
                var location = capture.IndexOf(everyXExpession);
                var everyXValue = int.Parse(capture.Substring(location + everyXExpession.Length));
                return GetRangeValues(index).Where(x => x % everyXValue == 0).ToArray();
            }
            else if (capture == "*")
            {
                return GetRangeValues(index).ToArray();
            }

            var value = ParseSingleValue(capture, index);
            return new int[] { value };
        }

        private static int ParseSingleValue(string capture, int index)
        {
            const int DayOfWeekIndex = 5;

            if (int.TryParse(capture, out var value))
            {
                return value;
            }
            else if (index == DayOfWeekIndex && DayOfWeekIndeces.ContainsKey(capture))
            {
                return DayOfWeekIndeces[capture];
            }
            else
            {
                throw new ArgumentException($"Unknown group: {capture}");
            }
        }

        private static readonly Dictionary<string, int> DayOfWeekIndeces = new()
        {
            {"MON", 1 }, {"TUE", 2}, {"WED", 3}, {"THU", 4}, {"FRI", 5}, {"SAT", 6}, {"SUN", 7}
        };

        private static IEnumerable<int> GetRangeValues(int index)
        {
            switch (index)
            {
                case 1: return Enumerable.Range(0, 60);
                case 2: return Enumerable.Range(0, 24);
                case 3: return Enumerable.Range(1, 31);
                case 4: return Enumerable.Range(1, 12);
                case 5: return Enumerable.Range(1, 7);
                default: throw new ArgumentOutOfRangeException(nameof(index));
            }
        }
    }
}
